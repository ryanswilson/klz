import klz
import numpy as np
import matplotlib.pyplot as plt
import multiprocessing
import cv2


def thresh_frame(frame, thresh):
    fs = frame.sum(axis=2)
    fsi = fs > thresh
    rf = np.zeros(fs.shape, dtype="B")
    rf[fsi] = 1
    return rf


def proc_frame(frame, thresh, xlims, ylims):
    fs = frame[xlims[0] : xlims[1], ylims[0] : ylims[1], :].sum(axis=2)
    fsi = fs > thresh
    rf = np.zeros(fs.shape, dtype="B")
    rf[fsi] = 1
    return rf


def load_movie(fname, thresh, lim=None):
    cap = cv2.VideoCapture(fname)
    frames = []
    i = 0
    go = True
    while cap.isOpened() and go:
        ret, frame = cap.read()
        frames.append(thresh_frame(frame, thresh))
        i += 1
        if lim:
            go = i < lim
    return frames


def np_frame_gen(frames, i, j, stride):
    nfs = [x[i : i + stride, j : j + stride] for x in frames]
    return klz.NpFrames(nfs)


# kernel encoding cost for n hits m misses for prob p
def tkd(p, n, m):
    return -np.log(p) * n - np.log(1 - p) * m


def eg(compable, p, mlen, sathe, sothe, sck, rethe):
    fv = compable.compress(p, mlen, sathe, sothe, sck, reth)
    rv = compable.reverse_compress(p, mlen, sathe, sothe, sck, reth)
    return rv - fv


def eg_map_help(vals):
    comp, inds, p, mlen, start_th, stop_th, slack, rth = vals
    return (eg(comp, p, mlen, start_th, stop_th, slack, rth), inds)


# entropy production images from frames
def image_from_frames(
    frames, stride, p, mlen, start_hits, stop_hits, slack, restart_hits, npool
):
    n = stride ** 2
    start_th = tkd(p, start_hits, n - start_hits) * 1.01
    rth = tkd(p, restart_hits, n - restart_hits) * 1.0
    stop_th = tkd(p, stop_hits, n - stop_hits) * 0.99
    fs = frames[0].shape
    inds = [[(i, j) for i in range(0, fs[0], stride)] for j in range(0, fs[1], stride)]
    frame_and_inds = [
        (np_frame_gen(frames, j[0], j[1], stride), j) for i in inds for j in i
    ]
    all_vals = [v + [p, mlen, start_th, stop_th, slack, rth] for v in frame_and_inds]

    with multiprocessing.Pool(npool) as p:
        ents = p.map(eg_map_help, all_vals)
    rvs = np.zeros(fs)
    for ent, ind in ents:
        rvs[ind[0], ind[1]] = ent
    return rvs


def movie_loader(fname, num, thresh, stride, xlims, ylims):
    n = stride ** 2
    cap = cv2.VideoCapture(fname)
    ret, frame = cap.read()

    interf = proc_frame(frame, thresh, xlims, ylims)
    ni = interf.shape[0] // stride
    nj = interf.shape[1] // stride
    print(ni, nj)
    if stride == 2:
        mv = klz.MovieBT(ni, nj, num)
    else:
        mv = klz.Movie(ni, nj, num)
    # TODO finish this
    mv.add_frame(interf, stride)

    i = 0
    go = True
    while cap.isOpened() and go:
        ret, frame = cap.read()
        if not ret:
            cap.set(cv2.CAP_PROP_POS_FRAMES, i + 1)
            i += 1
            print("bad frame:{} ".format(i))
            # return mv
        else:
            mv.add_frame(proc_frame(frame, thresh, xlims, ylims), stride)
            i += 1
            if num:
                go = i < num
    print("total frames: {}".format(i))
    return mv


def frame_from_movie(movie, fnum, stride):
    ms = movie.shape
    tarr = np.zeros((0, ms[1] * stride))
    for i in range(ms[0]):
        warr = np.zeros((stride, 0))
        for j in range(ms[1]):
            warr = np.append(warr, movie.get_sub((i, j), fnum), axis=1)
        tarr = np.append(tarr, warr, axis=0)
    return tarr
