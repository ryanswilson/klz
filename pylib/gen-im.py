import klz
import numpy as np
import matplotlib.pyplot as plt
import multiprocessing
import cv2

from helper import *

import sys
import time

args = sys.argv
n = int(args[1])
ptran = float(args[2])
stride = int(args[3])
ofile = args[4]
oim_file = args[5]
# TODO use proper command line args parser this is becoming a mess
custom_bounds = False
if len(args) > 6:
    custom_bounds = True
    x1 = int(args[6])
    x2 = int(args[7])
    y1 = int(args[8])
    y2 = int(args[9])

if len(args) > 10:
    rle = True
    print("\ncow\n")
else:
    rle = False

thresh = 41

rstart = time.time()

if custom_bounds:
    # good bounds for x,y include
    # 466,520 685,745 for funnle viewing
    # 300, 648), (400, 1000 for viewing the entire arena
    # 300 648 670 730 is the tall shot of the funnles
    frames = movie_loader(
        "/home/ryan/data/Fun038.avi", n * 2, thresh, stride, (x1, x2), (y1, y2)
    )
else:
    frames = movie_loader(
        "/home/ryan/data/Fun038.avi", n * 2, thresh, stride, (466, 520), (685, 745)
    )
rend = time.time()

print(rend - rstart)

start = time.time()
if rle:
    fim = frames.compress_rle(
        ptran,
        n,
        tkd(ptran, 3, 1) * 1.01,
        tkd(ptran, 3, 1) * 0.99,
        4,
        tkd(ptran, 4, 0) * 1.01,
    )
else:
    fim = frames.compress(
        ptran,
        n,
        tkd(ptran, 3, 1) * 1.01,
        tkd(ptran, 3, 1) * 0.99,
        4,
        tkd(ptran, 4, 0) * 1.01,
    )
end = time.time()
print("forward stage: ", end - start)
start = time.time()

if rle:
    rim = frames.reverse_compress_rle(
        ptran,
        n,
        tkd(ptran, 3, 1) * 1.01,
        tkd(ptran, 3, 1) * 0.99,
        4,
        tkd(ptran, 4, 0) * 1.01,
    )
else:
    rim = frames.reverse_compress(
        ptran,
        n,
        tkd(ptran, 3, 1) * 1.01,
        tkd(ptran, 3, 1) * 0.99,
        4,
        tkd(ptran, 4, 0) * 1.01,
    )
end = time.time()
print("reverse stage: ", end - start)

oim = rim - fim

vs = [round(x, 3) for col in oim for x in col]
v_str = ""
for v in vs:
    v_str += str(v) + ","
with open(ofile, "w") as f:
    f.write(v_str[:-1] + "\n")

plt.imshow(oim, interpolation=None)
plt.title(
    "Entropy Generation "
    + str(2 * n)
    + " "
    + str(ptran)
    + " "
    + str(stride)
    + " "
    + str(thresh)
)
plt.colorbar()

plt.savefig(oim_file)
plt.draw()
plt.pause(5)
# plt.show()
