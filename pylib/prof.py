import klz
import numpy as np
import matplotlib.pyplot as plt
import multiprocessing
import cv2

from datetime import datetime
import time

from helper import *

n = 1000
stride = 2
# frames=movie_loader("/home/ryan/data/Fun038.avi",n*2,70,stride,(300,648),(400,1000))
rstart = time.time()
frames = movie_loader(
    "/home/ryan/data/Fun038.avi", n * 2, 70, stride, (490, 500), (700, 710)
)
rend = time.time()

now = datetime.now()
dt_s = now.strftime("%d/%m/%Y %H:%M:%S")
rs = dt_s + " readtime: " + str(rend - rstart) + "\n"
print(rs)

ptran = 0.9
start = time.time()
cim = frames.compress(
    ptran,
    n,
    tkd(ptran, 3, 1) * 1.01,
    tkd(ptran, 3, 1) * 0.99,
    2,
    tkd(ptran, 4, 0) * 1.01,
)
end = time.time()


os = dt_s + " compress: " + str(end - start) + "\n"
with open("perf.txt", "a") as f:
    f.write(os)
    print(os)
    f.write(rs)


log_run = False
logfile = "vals.txt"
vs = [round(x, 3) for col in cim for x in col]
v_str = ""
for v in vs:
    v_str += str(v) + ","
if log_run:
    with open(logfile, "w") as f:
        f.write(v_str[:-1] + "\n")

    with open(logfile, "r") as f:
        ts = f.readline()
        vn = list(map(lambda x: float(x), ts.split(",")))
    print(vs)
    print(vn)


with open(logfile, "r") as f:
    ts = f.readline()
    vn = list(map(lambda x: float(x), ts.split(",")))
print(vn == vs)

plt.imshow(cim)
plt.show()
