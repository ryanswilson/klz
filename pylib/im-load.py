import klz
import numpy as np
import matplotlib.pyplot as plt
import multiprocessing
import cv2

from helper import *

import sys
import time


readfile = sys.argv[1]
ys = int(sys.argv[3])
xs = int(sys.argv[2])
title = sys.argv[4]
oim_file = sys.argv[5]


with open(readfile, "r") as f:
    ts = f.readline()
    vn = list(map(lambda x: float(x), ts.split(",")))

arr = np.reshape(vn, (xs, ys))

plt.imshow(arr, interpolation=None)
plt.colorbar()
plt.title(title)

plt.savefig(oim_file)
plt.show()
