// #[cfg(tests)]
mod klz_np_tests {
    use crate::klz::Kernel;
    use float_cmp::approx_eq;

    use super::super::*;

    #[test]
    fn test_thresh_kernel() {
        let m0 = array![[0, 0], [0, 0]];
        let m1 = array![[0, 1], [0, 0]];
        let m2 = array![[0, 1], [1, 0]];
        let m3 = array![[0, 1], [1, 1]];
        let m4 = array![[1, 1], [1, 1]];

        assert!(approx_eq!(
            f64,
            m0.dist(&m0, &vec!(0.9)),
            0.421,
            epsilon = 0.01
        ));
        assert!(approx_eq!(
            f64,
            m0.dist(&m1, &vec!(0.9)),
            2.619,
            epsilon = 0.01
        ));
        assert!(approx_eq!(
            f64,
            m0.dist(&m2, &vec!(0.9)),
            4.816,
            epsilon = 0.01
        ));
        assert!(approx_eq!(
            f64,
            m0.dist(&m3, &vec!(0.9)),
            7.013,
            epsilon = 0.01
        ));
        assert!(approx_eq!(
            f64,
            m0.dist(&m4, &vec!(0.9)),
            9.21,
            epsilon = 0.01
        ));
    }
}
