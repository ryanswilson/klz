use nalgebra::SVector;
use num_dual::*;
use std::ops::Index;
use std::vec::Vec;

#[cfg(test)]
#[path = "klz_test.rs"]
mod klz_tests;

pub trait Kernel {
    fn dist<D: DualNum<f64>>(&self, other: &Self, p: &Vec<D>) -> D;
}

#[derive(Debug, PartialEq)]
pub struct BestSequences {
    pub target_start: usize,
    pub costs: Vec<f64>,
    pub dict_starts: Vec<usize>,
}

impl BestSequences {
    pub fn join(&mut self, other: BestSequences) {
        let l = std::cmp::min(self.costs.len(), other.costs.len());

        for i in 0..l {
            if self.costs[i] > other.costs[i] {
                self.costs[i] = other.costs[i];
                self.dict_starts[i] = other.dict_starts[i];
            }
        }

        if other.costs.len() > self.costs.len() {
            for i in l..other.costs.len() {
                self.costs.push(other.costs[i]);
                self.dict_starts.push(other.dict_starts[i]);
            }
        }
    }
}

#[derive(Clone)]
enum GreedState {
    Fscan,
    Slack(usize),
}

pub enum Rmode {
    Forward,
    Backward,
}

pub struct Rslice<'a, T> {
    pub slice: &'a [T],
    pub rmode: Rmode,
}

impl<'a, T> Rslice<'a, T> {
    pub fn len(&self) -> usize {
        return self.slice.len();
    }
}

impl<'a, T> Index<usize> for Rslice<'a, T> {
    type Output = T;

    fn index(&self, i: usize) -> &Self::Output {
        match self.rmode {
            Rmode::Forward => {
                return &self.slice[i];
            }
            Rmode::Backward => {
                let l = self.len();
                return &self.slice[l - 1 - i];
            }
        }
    }
}

pub struct KLZ<'a, T: Kernel> {
    pub target: &'a [T],
    pub dict: Rslice<'a, T>,
    ind: usize,
    pub params: Vec<f64>,
}

impl<'a, T: Kernel> KLZ<'a, T> {
    pub fn new(target: &'a [T], dict: &'a [T], init_p: Vec<f64>) -> Self {
        return KLZ {
            target,
            dict: Rslice {
                slice: dict,
                rmode: Rmode::Forward,
            },
            ind: 0,
            params: init_p,
        };
    }

    fn greed_scan<D: DualNum<f64>>(
        &self,
        scan_start: usize,
        stop_thresh: f64,
        slack: usize,
        restart_thresh: f64,
        p: &Vec<D>,
    ) -> BestSequences {
        // TODO uhh ln or 2ln.....
        let base_cost = (self.dict.len() as f64).ln();
        let mut state = GreedState::Fscan;
        let mut cost_acc = base_cost;
        let mut seq = BestSequences {
            target_start: self.ind,
            costs: vec![],
            dict_starts: vec![],
        };
        let max_len_target = self.target.len() - self.ind;
        let max_len_dict = self.dict.len() - scan_start;
        for i in 0..std::cmp::min(max_len_dict, max_len_target) {
            match state {
                GreedState::Fscan => {
                    let step_cost = self.target[self.ind + i]
                        .dist(&self.dict[scan_start + i], p)
                        .re();
                    cost_acc += step_cost;
                    seq.costs.push(cost_acc.clone());
                    seq.dict_starts.push(scan_start);
                    if step_cost > stop_thresh {
                        state = GreedState::Slack(0);
                    }
                }
                GreedState::Slack(i) => {
                    let step_cost = self.target[self.ind + i]
                        .dist(&self.dict[scan_start + i], p)
                        .re();
                    cost_acc += step_cost;
                    seq.costs.push(cost_acc.clone());
                    seq.dict_starts.push(scan_start);
                    if step_cost < restart_thresh {
                        state = GreedState::Fscan;
                    } else {
                        if i < slack {
                            state = GreedState::Slack(i + 1)
                        } else {
                            break;
                        }
                    }
                }
            }
        }

        return seq;
    }

    pub fn greedy_sequencer(
        &self,
        start_thresh: f64,
        // start_neigherhood: usize,
        stop_thresh: f64,
        slack: usize,
        restart_thresh: f64,
    ) -> BestSequences {
        let dln = self.dict.len();
        let c = &self.target[self.ind];
        // let mut state = GreedState::Scan;

        let mut seq = BestSequences {
            target_start: self.ind,
            costs: vec![],
            dict_starts: vec![],
        };
        let wp: Vec<DualVec<f64, f64, 1_usize>> =
            self.params.iter().map(|&x| Dual64::from(x)).collect();

        //TODO implement start neighberhood
        for i in 0..dln {
            if c.dist(&self.dict[i], &wp).re < start_thresh {
                seq.join(self.greed_scan(
                    i,
                    stop_thresh,
                    slack,
                    restart_thresh,
                    &wp,
                ))
            }
        }

        return seq;
        // basic idea scans until finds match below start thresh
        //(could do a best in neighberhood)
        // then scans slack further after stop thresh,
        // restarting scan if below restart threshold
    }

    pub fn compress(
        &mut self,
        start_thresh: f64,
        stop_thresh: f64,
        slack: usize,
        restart_thresh: f64,
    ) -> f64 {
        self.ind = 0;
        let mut total_cost = 0.0;
        while self.ind != self.target.len() {
            let cur_seq = self.greedy_sequencer(
                start_thresh,
                stop_thresh,
                slack,
                restart_thresh,
            );
            let (steps, cost) = match cur_seq.costs.len() {
                //TODO review this logic (am i accounting for pointer cost correctly)
                //TODO when I ran pvar why on earth are ims quite different
                // why are there different numbers of zero length warnings
                0 => {
                    println!("zero length sequence: is it possible that you did not set the start thereshold low enough to find a match?");
                    (
                        1,0.0
			    //hard to decide what to do here 
                        // self.target[self.ind].dist(
                        //     &self.dict[self.target.len() - 1],
                        //     &self.params,
                        // ),
                    )
                }
                // (1,self.target[self.ind].dist(self.dict[], &self.params))},
                1 => (1, cur_seq.costs[0]),
                _ => {
                    let mut w_cost = cur_seq.costs[0];
                    let mut j = 1;
                    for i in 1..cur_seq.costs.len() {
                        if cur_seq.costs[i] / ((i + 1) as f64)
                            < w_cost / ((j + 1) as f64)
                        {
                            j = i + 1;
                            w_cost = cur_seq.costs[i];
                        }
                    }
                    (j, w_cost)
                }
            };
            total_cost += cost;
            self.ind += steps
        }

        return total_cost;
    }
}

impl<'a, T: std::cmp::PartialEq + Kernel> KLZ<'a, T> {
    //suspiciously many zero length matches (this is because start thresh was set to one off?)
    //blazing fast holy
    pub fn rle_compress(&mut self) -> f64 {
        self.ind = 0;
        let mut total_cost = 0.0;
        let base_cost = (self.dict.len() as f64).ln();
        let dict_len = self.dict.len();
        while self.ind < self.target.len() {
            let mut best_run = 0;
            let max_len_target = self.target.len() - self.ind;
            for i in 0..dict_len {
                let mut curr_run = 0;
                // TODO could probably write this loop better
                let max_len_dict = self.dict.len() - i;
                for j in 0..std::cmp::min(max_len_target, max_len_dict) {
                    if self.target[self.ind + j] == self.dict[i + j] {
                        curr_run += 1;
                    } else {
                        best_run = std::cmp::max(best_run, curr_run);
                        break;
                    }
                }
            }
            if best_run == 0 {
                self.ind += 1;
                // println!("Zero length match for rle")
            } else {
                self.ind += best_run;
                total_cost += base_cost;
            }
        }
        if self.ind > self.target.len() {
            println!("ind too high");
            // TODO debug here
        }

        return total_cost;
    }
}

//gradient per step should be part/part p (sum Wn * Vn * N)
//Wn is computed based on Vn only

impl<'a, T: Kernel, const R: usize> GradientStepper<R> for KLZ<'a, T> {
    fn gradient(&self) -> SVector<f64, R> {
        return SVector::zeros(); // TODO actually impl
    }

    fn param_step(&mut self, steps: SVector<f64, R>) {
        let np = self.params.len();

        if steps.len() == np {
            for i in 0..np {
                self.params[i] += steps[i]
            }
        } else {
            panic!("mismatched vector lengths")
        }
    }
}

pub trait GradientStepper<const R: usize> {
    fn gradient(&self) -> SVector<f64, R>;

    fn param_step(&mut self, steps: SVector<f64, R>);
}

pub struct AdamOptimizer<G: GradientStepper<R>, const R: usize> {
    pub model: G,
    m1_est: SVector<f64, R>,
    m2_est: SVector<f64, R>,
    beta1: f64,
    beta2: f64,
    steps: usize,
    rate: f64,
}

impl<G: GradientStepper<R>, const R: usize> AdamOptimizer<G, R> {
    pub fn new(model: G, beta1: f64, beta2: f64, rate: f64) -> Self {
        let m1v = SVector::zeros();
        let m2v = SVector::zeros();

        AdamOptimizer {
            model,
            beta1,
            beta2,
            m1_est: m1v,
            m2_est: m2v,
            steps: 0,
            rate,
        }
    }

    pub fn step(&mut self) -> () {
        let gt = self.model.gradient();
        self.m1_est = self.beta1 * self.m1_est + (1.0 - self.beta1) * gt;
        self.m2_est = self.beta2 * self.m2_est
            + (1.0 - self.beta2) * gt.component_mul(&gt);

        let m1s = self.m1_est / (1.0 - self.beta1.powf(self.steps as f64));
        let m2s = self.m2_est / (1.0 - self.beta2.powf(self.steps as f64));

        //TODO find an SIMD way to do this
        let mut td = SVector::zeros();
        for i in 0..R {
            td[i] = m2s[i].powf(-0.5) + 1.0e-8;
        }
        let nv = -self.rate * m1s * td;
        self.model.param_step(nv);
        self.steps += 1;
    }
}
