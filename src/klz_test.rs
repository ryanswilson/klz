// #[cfg(tests)]
mod klz_tests {
    use super::super::*;

    #[test]
    fn test_seq() {
        let mut a = low_seq();
        a.join(high_seq());
        let b = low_seq();
        assert_eq!(a, b);

        let mut a = high_seq();
        a.join(low_seq());
        let mut b = low_seq();
        b.dict_starts[2] = 5;
        b.dict_starts[3] = 5;
        b.target_start = 5;
        assert_eq!(a, b);
    }

    fn low_seq() -> BestSequences {
        BestSequences {
            target_start: 4,
            costs: vec![1.0, 2.0, 5.0, 1.0, 3.0, 4.0, 7.0],
            dict_starts: vec![1, 1, 2, 2, 4, 3, 1],
        }
    }

    fn high_seq() -> BestSequences {
        BestSequences {
            target_start: 5,
            costs: vec![2.0, 3.0, 5.0, 1.0, 4.0, 5.0],
            dict_starts: vec![5, 5, 5, 5, 5, 5],
        }
    }
}
