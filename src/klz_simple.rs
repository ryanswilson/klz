use crate::klz::{Kernel, Rmode};
use num_dual::*;
use pyo3::prelude::*;
use rand::prelude::*;
use std::vec::Vec;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum SState {
    A,
    B,
    C,
}

#[pyclass]
pub struct SFrames {
    pub frames: Vec<SState>,
}

impl Kernel for SState {
    fn dist<D: DualNum<f64>>(&self, other: &Self, p: &Vec<D>) -> D {
        if self == other {
            return p[0].ln().mul(-1.0);
        } else {
            return p[0].mul(-1.0).add(1.0).mul(0.5).ln().mul(-1.0);
        }
    }
}

#[pymethods]
impl SFrames {
    #[new]
    fn generate(p: f64, l: usize) -> Self {
        let mut rng = rand::thread_rng();
        let mut frames = vec![];
        let dist = rand::distributions::Uniform::new(0.0, 1.0);

        if l == 0 {
            return SFrames { frames };
        }
        let mut cur = match rng.gen_range(0..2) {
            0 => SState::A,
            1 => SState::B,
            _ => SState::C,
        };
        frames.push(cur.clone());
        for _ in 1..l {
            if dist.sample(&mut rng) < p {
                match &cur {
                    SState::A => cur = SState::B,
                    SState::B => cur = SState::C,
                    SState::C => cur = SState::A,
                }
            } else {
                match &cur {
                    SState::A => cur = SState::C,
                    SState::B => cur = SState::A,
                    SState::C => cur = SState::B,
                }
            }
            frames.push(cur.clone());
        }

        return SFrames { frames };
    }
    pub fn compress(
        &self,
        p: f64,
        mlen: usize,
        start_thresh: f64,
        stop_thresh: f64,
        slack: usize,
        restart_thresh: f64,
    ) -> f64 {
        let l = self.frames.len();
        let ll = std::cmp::min(l / 2, mlen);

        let mut klz_s = crate::klz::KLZ::new(
            self.frames[0..ll].into(),
            self.frames[ll..].into(),
            vec![p],
        );

        return klz_s.compress(
            start_thresh,
            stop_thresh,
            slack,
            restart_thresh,
        );
    }

    pub fn reverse_compress(
        &self,
        p: f64,
        mlen: usize,
        start_thresh: f64,
        stop_thresh: f64,
        slack: usize,
        restart_thresh: f64,
    ) -> f64 {
        let l = self.frames.len();
        let ll = std::cmp::min(l / 2, mlen);

        let mut klz_s = crate::klz::KLZ::new(
            self.frames[0..ll].into(),
            self.frames[ll..].into(),
            vec![p],
        );
        klz_s.dict.rmode = Rmode::Backward;
        return klz_s.compress(
            start_thresh,
            stop_thresh,
            slack,
            restart_thresh,
        );
    }
}
