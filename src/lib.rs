mod klz;
mod klz_np;
mod klz_np_bt;
mod klz_simple;
mod klz_stol;

use klz_np::{Movie, NpFrames};
use klz_np_bt::{MovieBT, NpFramesBT};
use klz_simple::SFrames;
use klz_stol::{StolRun, SubRun2};
use pyo3::prelude::*;

/// A Python module implemented in Rust.
#[pymodule]
fn klz(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<SubRun2>()?;
    m.add_class::<StolRun>()?;
    m.add_class::<SFrames>()?;
    m.add_class::<NpFrames>()?;
    m.add_class::<Movie>()?;
    m.add_class::<MovieBT>()?;
    Ok(())
}
