// #[cfg(tests)]
mod klz_stolif_tests {
    use crate::klz::Kernel;
    use float_cmp::approx_eq;

    use super::super::*;

    #[test]
    fn test_thresh_kernel() {
        let m0 = nalgebra::Matrix2::new(0, 0, 0, 0);
        let m1 = nalgebra::Matrix2::new(1, 0, 0, 0);
        let m2 = nalgebra::Matrix2::new(1, 1, 0, 0);
        let m3 = nalgebra::Matrix2::new(1, 1, 1, 0);
        let m4 = nalgebra::Matrix2::new(1, 1, 1, 1);

        assert!(approx_eq!(
            f64,
            m0.dist(&m0, &vec!(0.9)),
            0.421,
            epsilon = 0.01
        ));
        assert!(approx_eq!(
            f64,
            m0.dist(&m1, &vec!(0.9)),
            2.619,
            epsilon = 0.01
        ));
        assert!(approx_eq!(
            f64,
            m0.dist(&m2, &vec!(0.9)),
            4.816,
            epsilon = 0.01
        ));
        assert!(approx_eq!(
            f64,
            m0.dist(&m3, &vec!(0.9)),
            7.013,
            epsilon = 0.01
        ));
        assert!(approx_eq!(
            f64,
            m0.dist(&m4, &vec!(0.9)),
            9.21,
            epsilon = 0.01
        ));
    }

    #[test]
    fn compression_test() {
        let stol_run = StolRun::new_from_loc("data/run.mpak".to_string());
        let sr0 = stol_run.sub_run2(0, 0);
        assert_eq!(0.0, sr0.compress(0.9, 100, 0.43, 4.76, 5, 0.43));
    }
}
