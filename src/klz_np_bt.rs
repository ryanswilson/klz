use crate::klz::{Kernel, Rmode};
use ndarray::prelude::*;
use num_dual::*;
use numpy::{PyArray2, ToPyArray};
use pyo3::prelude::*;
use std::sync::{Arc, Mutex};
use std::vec::Vec;

// #[cfg(test)]
// #[path = "klz_np_tests.rs"]
// mod klz_np_tests;

struct I82 {
    v: u8,
}

impl I82 {
    pub fn from(v: Array2<u8>) -> Self {
        let mut mv = 0;
        if v[[0, 0]] == 1 {
            mv += 1;
        }
        if v[[0, 1]] == 1 {
            mv += 2;
        }
        if v[[1, 0]] == 1 {
            mv += 4;
        }
        if v[[1, 1]] == 1 {
            mv += 8;
        }

        return I82 { v: mv };
    }

    fn to(&self) -> Array2<u8> {
        let mut arr: Vec<u8> = vec![0; 4];
        arr[0] = (1 & self.v).count_ones() as u8;
        arr[1] = (2 & self.v).count_ones() as u8;
        arr[2] = (4 & self.v).count_ones() as u8;
        arr[3] = (8 & self.v).count_ones() as u8;
        return Array::from_shape_vec((2, 2).f(), arr).unwrap();
    }
}

struct I83 {
    v: u16,
}

impl I83 {
    pub fn from(v: Array2<u8>) -> Self {
        let mut mv = 0;
        for i in 0..3 {
            for j in 0..3 {
                if v[[i, j]] == 1 {
                    let ev = 3 * i + j;
                    mv += 1 << ev;
                }
            }
        }

        return I83 { v: mv };
    }

    fn to(&self) -> Array2<u8> {
        let mut arr: Vec<u8> = vec![0; 9];
        for i in 0..3 {
            for j in 0..3 {
                let ev = 3 * i + j;
                arr[3 * i + j] = (1 >> ev & self.v).count_ones() as u8;
            }
        }
        return Array::from_shape_vec((3, 3).f(), arr).unwrap();
    }
}

struct I84 {
    v: u16,
}

impl I84 {
    pub fn from(v: Array2<u8>) -> Self {
        let mut mv = 0;
        for i in 0..4 {
            for j in 0..4 {
                if v[[i, j]] == 1 {
                    let ev = 4 * i + j;
                    mv += 1 << ev;
                }
            }
        }

        return I84 { v: mv };
    }

    fn to(&self) -> Array2<u8> {
        let mut arr: Vec<u8> = vec![0; 9];
        for i in 0..4 {
            for j in 0..4 {
                let ev = 3 * i + j;
                arr[4 * i + j] = (1 >> ev & self.v).count_ones() as u8;
            }
        }
        return Array::from_shape_vec((4, 4).f(), arr).unwrap();
    }
}

pub struct NpFramesBT {
    frames: Vec<I82>,
}

impl NpFramesBT {
    pub fn compress(
        &self,
        p: f64,
        mlen: usize,
        start_thresh: f64,
        stop_thresh: f64,
        slack: usize,
        restart_thresh: f64,
    ) -> f64 {
        let l = self.frames.len();
        let ll = std::cmp::min(l / 2, mlen);

        let mut klz_s = crate::klz::KLZ::new(
            self.frames[0..ll].into(),
            self.frames[ll..].into(),
            vec![-p.ln(), -(1.0 - p).ln()],
        );

        return klz_s.compress(
            start_thresh,
            stop_thresh,
            slack,
            restart_thresh,
        );
    }

    pub fn reverse_compress(
        &self,
        p: f64,
        mlen: usize,
        start_thresh: f64,
        stop_thresh: f64,
        slack: usize,
        restart_thresh: f64,
    ) -> f64 {
        let l = self.frames.len();
        let ll = std::cmp::min(l / 2, mlen);

        let mut klz_s = crate::klz::KLZ::new(
            self.frames[0..ll].into(),
            self.frames[ll..].into(),
            vec![-p.ln(), -(1.0 - p).ln()],
        );
        klz_s.dict.rmode = Rmode::Backward;
        return klz_s.compress(
            start_thresh,
            stop_thresh,
            slack,
            restart_thresh,
        );
    }

    pub fn compress_rle(
        &self,
        _: f64,
        mlen: usize,
        _: f64,
        _: f64,
        _: usize,
        _: f64,
    ) -> f64 {
        let l = self.frames.len();
        let ll = std::cmp::min(l / 2, mlen);
        let p = 0.9;
        let mut klz_s = crate::klz::KLZ::new(
            self.frames[0..ll].into(),
            self.frames[ll..].into(),
            vec![-p.ln(), -(1.0 - p).ln()],
        );

        return klz_s.rle_compress();
    }

    pub fn reverse_compress_rle(
        &self,
        _: f64,
        mlen: usize,
        _: f64,
        _: f64,
        _: usize,
        _: f64,
    ) -> f64 {
        let l = self.frames.len();
        let ll = std::cmp::min(l / 2, mlen);
        let p = 0.9;
        let mut klz_s = crate::klz::KLZ::new(
            self.frames[0..ll].into(),
            self.frames[ll..].into(),
            vec![-p.ln(), -(1.0 - p).ln()],
        );
        klz_s.dict.rmode = Rmode::Backward;

        return klz_s.rle_compress();
    }

    #[allow(mutable_transmutes)]
    unsafe fn push_frame<'a, 'b>(&'a self, arr: Array2<u8>) -> () {
        let lol: &'b mut Self = std::mem::transmute(self);
        //there is probably a more rusty way, but this gets the job done
        lol.frames.push(I82::from(arr));
    }
}

#[pyclass]
pub struct MovieBT {
    pub cells: Vec<Vec<NpFramesBT>>,
    #[pyo3(get)]
    pub shape: (usize, usize),
}

#[pymethods]
impl MovieBT {
    #[new]
    pub fn new_empty(i: usize, j: usize, n: usize) -> Self {
        let mut mas_v = Vec::with_capacity(i);
        for _ in 0..i {
            let mut wv = Vec::with_capacity(j);
            for _ in 0..j {
                wv.push(NpFramesBT {
                    frames: Vec::with_capacity(n),
                });
            }
            mas_v.push(wv);
        }
        return MovieBT {
            cells: mas_v,
            shape: (i, j),
        };
    }

    pub fn add_frame(
        &mut self,
        frame: &PyArray2<u8>,
        stride: usize,
    ) -> PyResult<()> {
        let tarr_i = frame.readonly();
        let tarr = tarr_i.as_array();

        let pool = rayon::ThreadPoolBuilder::new()
            .num_threads(12)
            .build()
            .unwrap();

        let il = self.cells.len();
        let jl = self.cells[0].len();
        pool.install(|| frame_add_entry(self, &tarr, (il, jl), stride));

        return Ok(());
    }

    fn compress<'a>(
        &self,
        py: Python<'a>,

        p: f64,
        mlen: usize,
        start_thresh: f64,
        stop_thresh: f64,
        slack: usize,
        restart_thresh: f64,
    ) -> &'a PyArray2<f64> {
        let arr = self.compress_inner(
            NpFramesBT::compress,
            p,
            mlen,
            start_thresh,
            stop_thresh,
            slack,
            restart_thresh,
        );

        return arr.to_pyarray(py);
    }

    fn reverse_compress<'a>(
        &self,
        py: Python<'a>,

        p: f64,
        mlen: usize,
        start_thresh: f64,
        stop_thresh: f64,
        slack: usize,
        restart_thresh: f64,
    ) -> &'a PyArray2<f64> {
        let arr = self.compress_inner(
            NpFramesBT::reverse_compress,
            p,
            mlen,
            start_thresh,
            stop_thresh,
            slack,
            restart_thresh,
        );

        return arr.to_pyarray(py);
    }

    fn compress_rle<'a>(
        &self,
        py: Python<'a>,

        p: f64,
        mlen: usize,
        start_thresh: f64,
        stop_thresh: f64,
        slack: usize,
        restart_thresh: f64,
    ) -> &'a PyArray2<f64> {
        let arr = self.compress_inner(
            NpFramesBT::compress_rle,
            p,
            mlen,
            start_thresh,
            stop_thresh,
            slack,
            restart_thresh,
        );

        return arr.to_pyarray(py);
    }

    fn reverse_compress_rle<'a>(
        &self,
        py: Python<'a>,

        p: f64,
        mlen: usize,
        start_thresh: f64,
        stop_thresh: f64,
        slack: usize,
        restart_thresh: f64,
    ) -> &'a PyArray2<f64> {
        let arr = self.compress_inner(
            NpFramesBT::reverse_compress_rle,
            p,
            mlen,
            start_thresh,
            stop_thresh,
            slack,
            restart_thresh,
        );

        return arr.to_pyarray(py);
    }

    // #[getter]
    // pub fn frames(&self) -> PyResult<Arc<NpFrames>> {
    // return Ok(self.cells);
    // }
    pub fn get_sub(
        &self,
        py: Python,
        inds: (usize, usize),
        fnum: usize,
    ) -> Py<PyArray2<u8>> {
        let inter = self.cells[inds.0][inds.1].frames[fnum].to().to_pyarray(py);
        return inter.to_owned();
        // return Ok(self.cells[inds.0][inds.1]);
    }
}

impl MovieBT {
    pub fn compress_inner(
        &self,

        cfunc: fn(&NpFramesBT, f64, usize, f64, f64, usize, f64) -> f64,
        p: f64,
        mlen: usize,
        start_thresh: f64,
        stop_thresh: f64,
        slack: usize,
        restart_thresh: f64,
    ) -> Array2<f64> {
        let work_vec = vec![0.0; self.shape.0 * self.shape.1];
        //I believe unsafe is necessary, as compiler doesn't know that there
        // will be no surving refs

        let array_guard = Arc::new(Mutex::new(work_vec));
        let pool = rayon::ThreadPoolBuilder::new()
            .num_threads(12)
            .build()
            .unwrap();

        pool.install(|| {
            movie_compress_top_level(
                self,
                array_guard.clone(),
                cfunc,
                p,
                mlen,
                start_thresh,
                stop_thresh,
                slack,
                restart_thresh,
            )
        });

        let v = array_guard.lock().unwrap();
        let arr =
            Array::from_shape_vec((self.shape.0, self.shape.1), v.to_vec())
                .unwrap();
        return arr;
        // return arr.to_pyarray(py);
    }
}

fn movie_compress_top_level(
    movie: &MovieBT,
    output: Arc<Mutex<Vec<f64>>>,
    cfunc: fn(&NpFramesBT, f64, usize, f64, f64, usize, f64) -> f64,

    p: f64,
    mlen: usize,
    start_thresh: f64,
    stop_thresh: f64,
    slack: usize,
    restart_thresh: f64,
) -> () {
    rayon::scope(|s| {
        for i in 0..movie.shape.0 {
            let t_out = output.clone();
            s.spawn(move |_| {
                movie_compress_column(
                    movie,
                    t_out,
                    cfunc,
                    i,
                    p,
                    mlen,
                    start_thresh,
                    stop_thresh,
                    slack,
                    restart_thresh,
                )
            });
        }
    })
}

fn movie_compress_column(
    movie: &MovieBT,
    output: Arc<Mutex<Vec<f64>>>,
    cfunc: fn(&NpFramesBT, f64, usize, f64, f64, usize, f64) -> f64,
    i: usize,

    p: f64,
    mlen: usize,
    start_thresh: f64,
    stop_thresh: f64,
    slack: usize,
    restart_thresh: f64,
) -> () {
    rayon::scope(|s| {
        for j in 0..movie.shape.1 {
            let t_out = output.clone();
            s.spawn(move |_| {
                let frames = &movie.cells[i][j];

                let ent = cfunc(
                    &frames,
                    p,
                    mlen,
                    start_thresh,
                    stop_thresh,
                    slack,
                    restart_thresh,
                );
                let mut arr = t_out.lock().unwrap();
                arr[i * movie.shape.1 + j] = ent;
            });
        }
    })
}

fn frame_add_entry(
    movie: &MovieBT,
    // array: &Array2<u8>,
    array: &ArrayBase<ndarray::ViewRepr<&u8>, ndarray::Dim<[usize; 2]>>,
    mind: (usize, usize),
    stride: usize,
) -> () {
    rayon::scope(|s| {
        for i in 0..mind.0 {
            unsafe {
                s.spawn(move |_| {
                    frame_add_col(movie, array, mind.1, i, stride)
                });
            }
        }
    })
}

unsafe fn frame_add_col(
    movie: &MovieBT,
    // array: &Array2<u8>,
    array: &ArrayBase<ndarray::ViewRepr<&u8>, ndarray::Dim<[usize; 2]>>,
    rows: usize,
    i: usize,
    stride: usize,
) -> () {
    rayon::scope(|s| {
        for j in 0..rows {
            s.spawn(move |_| frame_add_single(movie, array, (i, j), stride));
        }
    });
}

unsafe fn frame_add_single(
    movie: &MovieBT,
    // array: &Array2<u8>,
    array: &ArrayBase<ndarray::ViewRepr<&u8>, ndarray::Dim<[usize; 2]>>,
    ind: (usize, usize),
    stride: usize,
) -> () {
    let (i, j) = ind;
    movie.cells[i][j].push_frame(
        array
            .slice(s![
                i * stride..(i + 1) * stride,
                j * stride..(j + 1) * stride
            ])
            .to_owned(),
    );
}

impl Kernel for I82 {
    fn dist<D: DualNum<f64>>(&self, other: &Self, p: &Vec<D>) -> D {
        let v = (self.v ^ other.v).count_ones() as f64;
        //number of misses

        //p[0]=-ln(p)
        //p[1]=-ln(1-p)
        let t0 = p[0].mul(4.0 - v);
        let t1 = p[1].mul(v);

        return t0 + t1;
    }
}

impl PartialEq for I82 {
    fn eq(&self, other: &Self) -> bool {
        return self.v == other.v;
    }
}

impl Kernel for I83 {
    fn dist<D: DualNum<f64>>(&self, other: &Self, p: &Vec<D>) -> D {
        let v = (self.v ^ other.v).count_ones() as f64;
        //number of misses

        //p[0]=-ln(p)
        //p[1]=-ln(1-p)
        let t0 = p[0].mul(9.0 - v);
        let t1 = p[1].mul(v);

        return t0 + t1;
    }
}

impl PartialEq for I83 {
    fn eq(&self, other: &Self) -> bool {
        return self.v == other.v;
    }
}

impl Kernel for I84 {
    fn dist<D: DualNum<f64>>(&self, other: &Self, p: &Vec<D>) -> D {
        let v = (self.v ^ other.v).count_ones() as f64;
        //number of misses

        //p[0]=-ln(p)
        //p[1]=-ln(1-p)
        let t0 = p[0].mul(16.0 - v);
        let t1 = p[1].mul(v);

        return t0 + t1;
    }
}

impl PartialEq for I84 {
    fn eq(&self, other: &Self) -> bool {
        return self.v == other.v;
    }
}
