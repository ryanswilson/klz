use nalgebra::SMatrix;
use num_dual::*;
use pyo3::prelude::*;
use serde::Deserialize;
use std::vec::Vec;

#[cfg(test)]
#[path = "klz_stolif_tests.rs"]
mod klz_stolif_tests;

/// This class exists to deserialize runs from stolif/ other programs that produce
/// a message pack contain thresholded movies
#[pyclass]
#[derive(Debug, Deserialize)]
pub struct StolRun {
    frames: Vec<Vec<Vec<u8>>>,
}

#[pymethods]
impl StolRun {
    /// initialization with location of mpack with movie
    #[new]
    pub fn new_from_loc(loc: String) -> Self {
        let mut file = std::io::BufReader::new(
            std::fs::File::open(loc).expect("bad read"),
        );
        let stol_run_res = rmp_serde::decode::from_read(file);

        return stol_run_res.expect("non conforming run");
    }

    /// returns a "SubRun2" which has the analysis methods
    pub fn sub_run2(&self, i: usize, j: usize) -> SubRun2 {
        let mut rv = vec![];
        for frame in self.frames.iter() {
            rv.push(nalgebra::Matrix2::from_fn(|r, c| frame[i + r][j + c]))
        }

        return SubRun2 {
            inner: SubRun { frames: rv },
        };
    }
}

struct SubRun<const R: usize> {
    frames: Vec<SMatrix<u8, R, R>>,
}

impl<const R: usize> SubRun<R> {
    fn compress(
        &self,
        p: f64,
        mlen: usize,
        start_thresh: f64,
        stop_thresh: f64,
        slack: usize,
        restart_thresh: f64,
    ) -> f64 {
        //TODO probably bad form creating a copy of all frames for the KLZ class
        //should somehow make them more synergistic, espcially for long running

        let l = self.frames.len();
        let ll = std::cmp::min(l / 2, mlen);

        let mut klz_s = crate::klz::KLZ::new(
            self.frames[0..ll].into(),
            self.frames[ll..].into(),
            vec![p],
        );

        return klz_s.compress(
            start_thresh,
            stop_thresh,
            slack,
            restart_thresh,
        );
    }
}

impl<const R: usize> crate::klz::Kernel for SMatrix<u8, R, R> {
    fn dist<D: DualNum<f64>>(&self, other: &Self, p: &Vec<D>) -> D {
        let mut t = self + other;
        t = t.map(|x| match x {
            1 => return 1,
            _ => return 0,
        });

        let v = t.fold(0, |acc, v| acc + v) as f64;
        let mv = (R * R) as f64;
        let t0 = p[0].ln().mul(v - mv);
        let t1 = p[0].mul(-1.0).add(1.0).ln().mul(-v);

        return t0 + t1;
    }
}

/// hacky way to wrap the SubRun which access the klz methods
#[pyclass]
pub struct SubRun2 {
    inner: SubRun<2>,
}

#[pymethods]
impl SubRun2 {
    pub fn compress(
        &self,
        p: f64,
        mlen: usize,
        start_thresh: f64,
        stop_thresh: f64,
        slack: usize,
        restart_thresh: f64,
    ) -> f64 {
        return self.inner.compress(
            p,
            mlen,
            start_thresh,
            stop_thresh,
            slack,
            restart_thresh,
        );
    }
}
