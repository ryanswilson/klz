## KLZ
KLZ, or Kernel Lempel Ziv, is an attempt to modify the technique to calculate entropy production in Chaikins paper in a way that makes it more amenable to smaller data sets.

### Motivation
Naive LZ compression needs a data set that is exponentially large in the effective state space of the process that is being compressed. This necessitates the use of a 2x2 grid for real experimental data, which Chaikins papers shows to be undesirable as it severely underestimates the produced entropy in many scenarios. Further even when using the 2x2 grid size it is clear that compression algorithm is very far from convergence to the asymptotic compression ratio, as the plot is extremely noisy.

### Proposed Plan, 
#### why its called KLZ
The main idea is that a perfect match between symbols should not be necessary during the compression step but rather that there is some underlying metric between the symbols, some are closer to other. If in a 4x4 array of threshold-ed pixels there is a perfect match except for one pixel which is different, maybe it should be used with some penalty instead of completely restarting the match chain in the LZ algorithm. 

Practicaly this is done by defining a kernel over the symbols which is a function of some parameter vector $`\theta`$, where the kernel is some probability distribution of transitions between the symbols. This naturally leads to a new compression algorithm, and thus a measure of entropy production. However for every value of $`\theta`$ there is some limit to compression that is not the optimal asymptotic compression. Thus the parameters will be tuned, balancing the trade-off of a more permissive kernel which gets better compression by restarting fewer times but has worse compression because of the cost payed at every step to encode the transition used by the kernel. 


### Using
To build the program you need to have rust/cargo installed. Then you need to create a virtual env and source it. For example
```
python -m venv venv
source venv/bin/activate
```
Then you need to install maturin using pip, maturin is used to create the python package.
```
maturin develop
```
Finally you can now use the library from python. Currently not much of the rust API is exposed, you could examine the source code and look for the #[py*] macro blocks or examine the file klz_tests to see usage examples.

``` python
import klz
stol_run= klz.StolRun("data/run.mpak")
sr2=stol_run.sub_run2(0,0)
print(sr2.compress(.9,100,0.43,4.76,5,0.43))
```
